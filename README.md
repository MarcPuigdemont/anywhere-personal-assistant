# Anywhere Personal Assistant

> An electron-vue project

#### Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build

# run unit & end-to-end tests
npm test

# lint all JS/Vue component files in `src/`
npm run lint
```

# TODO List:

- Add index to versions on settings to reinforce order
- Checkout screen
- Frontport Screen (Prototype is done)
- Add 'loading' to ticket and branches
- Drawer pipeline for postponed actions: checkout frankfurt.next -> pull -> checkout ticket branches -> pull
- Add colors of versions to Settings
- Work with multiple folders (Laura)
- ~~Link to open PR, and link to open branch on BB~~
- ~~Improve settings with tabs~~
- ~~Custom feature branches~~, now support for ticket checkout on feature branches
- ~~Settings screen~~
- ~~Make dialog not show if no affected repo is changed~~
- ~~Put versions on a single place and make the conversion develop -> Honolulu~~
- ~~Sort versions on branches, checkout buttons, etc.~~
- ~~Make branch arrow clickable (change icon on mouse over) to allow checkout only that branch~~
- ~~(Won't do)TicketBranchesCheckout, switching to branch on a disabled repo should show a warning~~

---

This project was generated with [electron-vue](https://github.com/SimulatedGREG/electron-vue)@[8fae476](https://github.com/SimulatedGREG/electron-vue/tree/8fae4763e9d225d3691b627e83b9e09b56f6c935) using [vue-cli](https://github.com/vuejs/vue-cli). Documentation about the original structure can be found [here](https://simulatedgreg.gitbooks.io/electron-vue/content/index.html).
