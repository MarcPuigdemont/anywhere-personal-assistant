import utils from '../utils';

describe('Launch', function() {
  beforeEach(utils.beforeEach);
  afterEach(utils.afterEach);

  it('shows the proper application title', function() {
    return this.app.client
      .getTitle()
      .then(title => {
        expect(title).to.equal('Anywhere Personal Assistant');
        return this.app.client.element('.v-overlay--active');
      })
      .then(element => {
        if (element.status === 0) {
          this.app.client.click('.v-overlay--active');
        }
        console.log(element);
      });
  });
});
