import { statSync, readFileSync, writeFileSync } from 'fs';
import { exec } from 'child_process';

import { constants } from '../../../src/renderer/utils/constants';
import { parseRepos, parseRepoFile } from '../../../src/renderer/utils/command-utils';
import execAsync from '../../../src/renderer/utils/os-utils';

const state = {
  repos: [],
  repoFileLastUpdate: null,
  performingAction: false,
  dialogOpen: false,
  dialogResult: undefined,
};

const mutations = {
  UPDATE_REPOS(state, payload) {
    const currentRepos = state.repos;
    payload.forEach(element => {
      const index = currentRepos.map(repo => repo.name).indexOf(element.name);
      if (index > -1) {
        currentRepos[index] = Object.assign(currentRepos[index], element);
      } else {
        currentRepos.push(element);
      }
    });

    console.log(currentRepos);
    state.repos = Array.from(currentRepos);
  },
  SET_PERFORM_ACTION(state, payload) {
    state.performingAction = payload;
  },
  SET_DIALOG_OPEN(state, payload) {
    state.dialogOpen = payload;
  },
  SET_DIALOG_RESULT(state, payload) {
    state.dialogResult = payload;
  },
  SET_REPOFILE_LAST_UPDATE(state, payload) {
    state.repoFileLastUpdate = payload;
  },
};

const getters = {
  dialogResult: state => state.dialogResult,
  repos: state => state.repos,
  dirtyAndEnabledRepos: state => state.repos.filter(repo => repo.enabled && !!repo.clean),
  repoFileLastUpdate: state => state.repoFileLastUpdate,
};

const actions = {
  /**
   * Gets the repos from the repo file, if they exist, it updates their enabled property
   * @param {*} store
   */
  reloadRepos({ commit }) {
    commit('SET_PERFORM_ACTION', true);
    const data = readFileSync(`${constants.ANYWHERE_META}repos.txt`, 'utf8');
    const metadata = statSync(`${constants.ANYWHERE_META}repos.txt`);
    commit('SET_REPOFILE_LAST_UPDATE', metadata.mtime);
    commit('UPDATE_REPOS', parseRepoFile(data));
    commit('SET_PERFORM_ACTION', false);
  },
  /**
   * Perform a status command and updates repos
   * @param {*} store
   */
  reposStatus({ commit }) {
    commit('SET_PERFORM_ACTION', true);
    return new Promise((resolve, reject) => {
      execAsync(`cd ${constants.ANYWHERE_META} && ${constants.METASCRIPTS}/status.sh`).then(({ stderr, stdout }) => {
        if (stderr) {
          commit('SET_PERFORM_ACTION', false);
          reject(stderr);
        } else {
          commit('UPDATE_REPOS', parseRepos(stdout));
          commit('SET_PERFORM_ACTION', false);
          resolve();
        }
      });
    });
  },
  /**
   * Saves to disk the changes on repos to the repo file
   * @param {*} store
   * @param {{repo:string, enabled:boolean}} repo
   */
  saveRepoFile({ dispatch }, repo) {
    const findRepoOnFile = (repo, lines) =>
      lines.findIndex(line => {
        const name = line.split(':')[0].replace('#', '');
        return repo.repo === name;
      });

    const metadata = statSync(`${constants.ANYWHERE_META}repos.txt`);
    const data = readFileSync(`${constants.ANYWHERE_META}repos.txt`, 'utf8');
    const lines = data.split('\n');
    const index = findRepoOnFile(repo, lines);
    if (new Date(metadata.mtime) > new Date(this.getters.repoFileLastUpdate)) {
      const fileRepoEnabled = lines[index][0] !== '#';
      if (repo.enabled !== fileRepoEnabled) {
        console.error('Error, file modified and sync lost');
        return;
      }
    }

    if (repo.enabled) {
      lines[index] = `#${lines[index]}`;
    } else {
      lines[index] = lines[index].substr(1, lines[index].length);
    }

    writeFileSync(`${constants.ANYWHERE_META}repos.txt`, lines.join('\n'), { encoding: 'utf8' });
    dispatch('reloadRepos');
  },
  /**
   * Opens a dialog asking for permission to continue if the repos supplied or the
   * store repos are enabled and not clean
   * @param {*} store
   * @param {[{enabled:boolean, clean:boolean}]=} payload
   */
  openDialog({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.dispatch('reposStatus')
        .then(() => {
          // TODO payload will be a repo list with the repos to check or empty
          // empty will check for all enabled repos
          console.log(payload);
          const repos = payload || this.getters.repos;
          const dirtyRepos = repos.filter(repo => repo.enabled && !repo.clean);
          console.log('dirty repos: ', dirtyRepos);
          if (dirtyRepos.length === 0) {
            resolve();
            return;
          }
          commit('SET_DIALOG_OPEN', true);
          const unwatch = this.watch(
            () => this.getters.dialogResult,
            newVal => {
              commit('SET_DIALOG_OPEN', false);
              unwatch();
              commit('SET_DIALOG_RESULT', undefined);

              if (newVal) {
                resolve();
              } else {
                reject();
              }
            },
          );
        })
        .catch(reject);
    });
  },
  /**
   * Takes a list of custom repo objects and checks out to all of them
   * @param {*} store
   * @param {[{name: string, branch: string}]} repos
   */
  gitCheckout({ commit, dispatch }, repos) {
    commit('SET_PERFORM_ACTION', true);
    setTimeout(() => {
      const promises = [];
      repos.forEach(repo => {
        promises.push(
          new Promise(async resolve => {
            const existBranch = await dispatch('existBranchLocally', {
              repo: repo.name,
              branch: repo.branch,
            });
            let command;
            if (existBranch) {
              command =
                `cd ${constants.ANYWHERE_META} && ` +
                `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${repo.branch}`;
            } else {
              command =
                `cd ${constants.ANYWHERE_META} && ` +
                `${constants.METASCRIPTS}/repo.sh ${repo.name} fetch && ` +
                `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${repo.branch}`;
            }
            const child = exec(command);
            child.on('close', code => {
              console.log(`Finished repo.sh for branch ${repo.name} with code ${code}`);
              resolve();
              // TODO if close code error, reject
            });
          }),
        );
      });
      Promise.all(promises).then(() => dispatch('reposStatus'));
    }, 1000);
  },
  /**
   * Checks out the repos supplied to the branch specified by the parameter
   * @param {*} store
   * @param {{ repos: {enabled:boolean, branch:string, name:string}, branch:string}} payload
   */
  gitAllCheckout({ commit, dispatch }, { repos, branch }) {
    commit('SET_PERFORM_ACTION', true);
    setTimeout(() => {
      const promises = [];
      repos.forEach(repo => {
        if (repo.enabled && repo.branch !== branch) {
          promises.push(
            new Promise(async resolve => {
              const existBranch = await dispatch('existBranchLocally', { repo: repo.name, branch });
              let command;
              if (existBranch) {
                command =
                  `cd ${constants.ANYWHERE_META} && ` +
                  `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${branch}`;
              } else {
                command =
                  `cd ${constants.ANYWHERE_META} && ` +
                  `${constants.METASCRIPTS}/repo.sh ${repo.name} fetch && ` +
                  `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${branch}`;
              }
              const child = exec(command);
              child.stderr.on('data', data => {
                if (data.indexOf('POST-CHECKOUT') > -1) {
                  console.log(`Detected branch switch for repo ${repo.name}`);
                  repo.branch = branch;
                  commit('UPDATE_REPOS', [repo]);
                }
              });
              child.on('close', code => {
                console.log(`Finished repo.sh for branch ${repo.name} with code ${code}`);
                resolve();
                // TODO if close code error, reject
              });
            }),
          );
        }
      });
      Promise.all(promises).then(() => dispatch('reposStatus'));
    }, 1000);
  },
  /**
   * Performs a fetch && merge command
   * @param {*} store
   */
  fetchAndMerge({ commit }) {
    commit('SET_PERFORM_ACTION', true);
    return new Promise(resolve => {
      execAsync(
        `cd ${constants.ANYWHERE_META} && ${constants.METASCRIPTS}/fetch.sh && ${
          constants.METASCRIPTS
        }/gitall.sh merge`,
      ).then(() => {
        console.log('fetch && merge finished');
        commit('SET_PERFORM_ACTION', false);
        resolve();
      });
    });
  },
  /**
   * Checks if a branch exists locally without doing any request, just local git
   * @param {*} store
   * @param {{repo: string, branch: string}}} name of the repo and name of the branch
   */
  existBranchLocally({ commit }, { repo, branch }) {
    commit('SET_PERFORM_ACTION', true);
    return new Promise(resolve => {
      const child = exec(
        `cd ${constants.ANYWHERE_META} && ${constants.METASCRIPTS}/repo.sh ${repo} rev-parse --verify ${branch}`,
      );
      child.on('close', code => {
        commit('SET_PERFORM_ACTION', false);
        resolve(code === 0);
      });
    });
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
