/* eslint-disable global-require */
/* eslint-disable import/no-webpack-loader-syntax */

describe('os-utils', () => {
  let osUtilsInjector;
  beforeEach(() => {
    osUtilsInjector = require('inject-loader!@/utils/os-utils');
  });

  it('execAsync should resolve with results of exec', () => {
    const osUtils = osUtilsInjector({
      child_process: {
        exec: (command, options, callback) => {
          callback(`errors don't matter`, 'success');
        },
      },
    });

    const execAsync = osUtils.default;
    return execAsync('command').then(result => {
      expect(result.stdout).to.equal('success');
      expect(result.stderr).to.equal(`errors don't matter`);
    });
  });
});
