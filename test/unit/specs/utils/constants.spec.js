/* eslint-disable global-require */
/* eslint-disable import/no-webpack-loader-syntax */

describe('constants file', () => {
  let constantsInjector;
  beforeEach(() => {
    constantsInjector = require('inject-loader!@/utils/constants');
  });

  describe('constants', () => {
    it('should have all mandatory attributes', () => {
      const constants = constantsInjector({
        fs: {
          readFileSync: () => {},
        },
      });

      const mandatoryFields = [
        'METASCRIPTS',
        'ANYWHERE_META',
        'ANYWHERE_VERSIONS',
        'ANYWHERE_FEATURE_BRANCHES',
        'BB_USER',
        'BB_PASSWORD',
        'JIRA_USER',
        'JIRA_PASSWORD',
        'BRANCH_USER',
        'USER_DATA_PATH',
        'CONSTANTS_FILE_PATH',
      ];

      const fields = new Set(Object.keys(constants.constants));
      // -1 because of constants.config function
      const expectedNumberOfFields = Object.keys(constants.constants).length - 1;

      expect(expectedNumberOfFields).to.equal(mandatoryFields.length);
      expect(mandatoryFields.every(value => fields.has(value))).to.equal(true);
    });

    it('should not load anything if file is not found', () => {
      /* eslint-disable no-throw-literal */
      const constants = constantsInjector({
        fs: {
          readFileSync: () => {
            throw 'file not found';
          },
        },
      });

      constants.constants.configure('home path', 'app name');
      const values = Object.values(constants.constants);
      expect(values.every(value => typeof value === 'function' || value === '')).to.equal(true);
    });

    it('should load from settings file', () => {
      const settingsFileContents = {
        metascriptsPath: 'metascriptsPath',
        anywherePath: 'anywherePath',
        anywhereVersions: 'anywhereVersions',
        featureBranches: 'featureBranches',
        bbUser: 'bbUser',
        bbPassword: 'bbPassword',
        jiraUser: 'jiraUser',
        jiraPassword: 'jiraPassword',
        branchUser: 'branchUser',
        userDataPath: 'userDataPath',
        filePath: 'filePath',
      };
      const homePath = 'homePath';
      const appName = 'appName';
      const constants = constantsInjector({
        fs: {
          readFileSync: () => JSON.stringify(settingsFileContents),
        },
      });
      const userDataPath = `${homePath}/Library/Application Support/${appName}`;
      const filePath = `${userDataPath}/settings.js`;

      constants.constants.configure(homePath, appName);

      expect(constants.constants.METASCRIPTS).to.equal(settingsFileContents.metascriptsPath);
      expect(constants.constants.ANYWHERE_META).to.equal(settingsFileContents.anywherePath);
      expect(constants.constants.ANYWHERE_VERSIONS).to.equal(settingsFileContents.anywhereVersions);
      expect(constants.constants.ANYWHERE_FEATURE_BRANCHES).to.equal(settingsFileContents.featureBranches);
      expect(constants.constants.BB_USER).to.equal(settingsFileContents.bbUser);
      expect(constants.constants.BB_PASSWORD).to.equal(settingsFileContents.bbPassword);
      expect(constants.constants.JIRA_USER).to.equal(settingsFileContents.jiraUser);
      expect(constants.constants.JIRA_PASSWORD).to.equal(settingsFileContents.jiraPassword);
      expect(constants.constants.BRANCH_USER).to.equal(settingsFileContents.branchUser);
      expect(constants.constants.USER_DATA_PATH).to.equal(userDataPath);
      expect(constants.constants.CONSTANTS_FILE_PATH).to.equal(filePath);
    });
  });

  describe('version comparator', () => {
    it('should sort properly', () => {
      const constantsValues = {
        anywhereVersions: [
          { name: 'frankfurt', isDevelop: false },
          { name: 'granada', isDevelop: false },
          { name: 'honolulu', isDevelop: true },
        ],
      };

      const constants = constantsInjector({
        fs: {
          readFileSync: () => JSON.stringify(constantsValues),
        },
      });
      constants.constants.configure('homePath', 'appName');

      expect(constants.versionComparator('frankfurt', 'granada')).to.equal(-1);
      expect(constants.versionComparator('frankfurt', 'develop')).to.equal(-2);
      expect(constants.versionComparator('granada', 'develop')).to.equal(-1);

      expect(constants.versionComparator('granada', 'frankfurt')).to.equal(1);
      expect(constants.versionComparator('develop', 'frankfurt')).to.equal(2);
      expect(constants.versionComparator('develop', 'granada')).to.equal(1);

      expect(constants.versionComparator('frankfurt', 'frankfurt')).to.equal(0);
      expect(constants.versionComparator('granada', 'granada')).to.equal(0);
      expect(constants.versionComparator('develop', 'develop')).to.equal(0);
    });
  });

  describe('branches comparator', () => {
    it('should sort properly', () => {
      const constantsValues = {
        anywhereVersions: [
          { name: 'frankfurt', isDevelop: false },
          { name: 'granada', isDevelop: false },
          { name: 'honolulu', isDevelop: true },
        ],
      };

      const constants = constantsInjector({
        fs: {
          readFileSync: () => JSON.stringify(constantsValues),
        },
      });
      constants.constants.configure('homePath', 'appName');

      expect(constants.branchesComparator('feature/develop/marcp/AW-1234', 'feature/frankfurt/marcp/AW-1234')).to.equal(
        -1,
      );
      expect(constants.branchesComparator('hotfix/granada/marcp/AW-1234', 'hotfix/frankfurt/marcp/AW-1234')).to.equal(
        1,
      );
      expect(constants.branchesComparator('release/frankfurt.next', 'feature/frankfurt/marcp/AW-1234')).to.equal(1);
      expect(constants.branchesComparator('feature/frankfurt/marcp/AW-1234', 'release/frankfurt.next')).to.equal(-1);
      expect(constants.branchesComparator('release/frankfurt.next', 'release/granada.next')).to.equal(-1);
      expect(constants.branchesComparator('release/frankfurt.next', 'develop')).to.equal(-2);
    });
  });
});
