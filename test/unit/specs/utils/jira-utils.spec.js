/* eslint-disable global-require */
import getTickets from '@/utils/jira-utils.js';

describe('jira-utils', () => {
  it('getTickets should return issues property', () => {
    const test = { issues: 'issues' };
    const result = getTickets(test);
    expect(result).to.equal('issues');
  });
});
