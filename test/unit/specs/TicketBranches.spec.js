import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.css';

import TicketBranchesOriginal from '@/components/Tickets/TicketBranches';

Vue.use(Vuetify);
Vue.use(Vuex);
if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

describe('TicketBranches.vue', () => {
  let TicketBranchesInjector;
  beforeEach(() => {
    // eslint-disable-next-line import/no-webpack-loader-syntax
    TicketBranchesInjector = require('!!vue-loader?inject!@/components/Tickets/TicketBranches.vue');
  });

  it('should render correct contents', () => {
    const TicketBranches = TicketBranchesInjector({
      '../../utils/constants.js': {
        constants: {
          ANYWHERE_VERSIONS: [],
        },
        versionComparator: () => 0,
      },
      '../../utils/os-utils': {
        execAsync: () => {},
      },
    });
    TicketBranches.render = TicketBranchesOriginal.render;
    TicketBranches.staticRenderFns = TicketBranchesOriginal.staticRenderFns;

    const Component = Vue.extend(TicketBranches);
    const vm = new Component({
      propsData: {
        ticket: 'AW-1234',
        branches: [
          {
            component: 'core',
            type: 'hotfix',
            level: 'frankfurt',
            title: 'test branch',
          },
          {
            component: 'editablecontrols',
            type: 'frontport',
            level: 'granada',
            title: 'test branch 2',
          },
        ],
      },
    }).$mount();

    expect(vm.$el.querySelectorAll('.branch').length).to.equal(2);
    expect(vm.$el.querySelectorAll('.layout.row').length).to.equal(2);

    const firstBranch = vm.$el.querySelectorAll('.branch')[0];
    const branchElements = firstBranch.querySelectorAll('.flex');
    // 1 is sourcetree component
    expect(branchElements[2].textContent).to.equal('core');
    expect(branchElements[3].textContent).to.equal('hotfix');
    expect(branchElements[4].textContent).to.equal('frankfurt');
    expect(branchElements[5].textContent).to.equal('test branch');
    expect(branchElements.length).to.equal(6);
  });
});
