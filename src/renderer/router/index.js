import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'repos',
      component: require('@/components/Repos').default,
    },
    {
      path: '/prs',
      name: 'prs',
      component: require('@/components/PullRequests').default,
    },
    {
      path: '/tickets',
      name: 'tickets',
      component: require('@/components/Tickets').default,
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('@/components/Settings').default,
    },
    {
      path: '/frontport',
      name: 'frontport',
      component: require('@/components/Frontport').default,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
