import { exec } from 'child_process';
import axios from 'axios';

import { parseJiraResponse } from '../../utils/command-utils';
import { constants } from '../../utils/constants';

const state = {
  tickets: [],
};

const mutations = {
  UPDATE_TICKETS(state, payload) {
    if (state.tickets.length > 0) {
      state.tickets = payload.issues.map(ticket => {
        const previousTicket = state.tickets.find(t => t.key === ticket.key);
        if (previousTicket) {
          return { ...ticket, branches: previousTicket.branches };
        }
        return ticket;
      });
    } else {
      state.tickets = payload.issues;
    }
  },
  UPDATE_TICKET_WITH_BRANCHES(state, { ticket: key, branches }) {
    const tickets = state.tickets;
    const index = tickets.findIndex(t => t.key === key);
    if (index > -1) {
      tickets[index].branches = branches;
      state.tickets = Array.from(tickets);
    }
  },
};

const actions = {
  /**
   *
   * @param {*} store
   * @param {{ user:string, password:string }} credentials
   */
  jiraGetTickets({ commit }, { user, password, accountId }) {
    return new Promise(resolve => {
      const command = `\
        curl \
          -D- \
          -u ${user}:${password} \
          -X GET \
          -H "Content-Type: application/json" \
          https://cetrea.jira.com/rest/api/3/search?jql=assignee=${accountId}+and+resolution+is+empty+order+by+updated \
      `;

      const child = exec(command);
      let result = '';
      child.stdout.on('data', data => {
        result += data;
      });
      child.on('close', code => {
        console.log(`Finished jiraGetTickets with code ${code}`);
        commit('UPDATE_TICKETS', parseJiraResponse(result));
        resolve();
      });
    });
  },
  bitbucketGetBranches({ commit }, { repos, key }) {
    return new Promise(resolve => {
      console.log(`getting branches of ${key}`, repos);
      const promises = [];
      repos.forEach(repo => {
        promises.push(
          new Promise(innerResolve => {
            axios
              .get(`https://bitbucket.org/api/2.0/repositories/CETREA/${repo.repo}/refs/branches?q=name ~ "${key}"`, {
                auth: {
                  username: constants.BB_USER,
                  password: constants.BB_PASSWORD,
                },
              })
              .then(response => {
                if (response.data.next) {
                  console.error(response);
                }
                innerResolve(response.data.values);
              });
          }),
        );
      });

      Promise.all(promises).then(listOfBranchesLists => {
        const result = listOfBranchesLists.reduce((memo, item) => memo.concat(item), []);
        console.log('fetched branches', result);
        const branches = result.map(branch => {
          const component = branch.target.repository.name.replace('Anywhere ', '');
          const parts = branch.name.split('/');
          const type = parts[0];
          const level = parts.length > 1 ? parts[1] : 'Unknown';
          const author = parts.length > 2 ? parts[2] : 'Unknown';
          const title = parts.length > 3 ? parts[3] : 'Unknown';
          return { component, title, type, level, author, name: branch.name };
        });

        commit('UPDATE_TICKET_WITH_BRANCHES', { ticket: key, branches });
        resolve(branches);
      });
    });
  },
};

export default {
  state,
  mutations,
  actions,
};
