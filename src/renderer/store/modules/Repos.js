import { statSync, readFileSync, writeFileSync } from 'fs';
import { exec } from 'child_process';

import { constants } from '../../utils/constants';
import { parseRepos, parseRepoFile } from '../../utils/command-utils';
import execAsync from '../../utils/os-utils';

/** @module storeRepos */

/**
 * A repo.
 * @typedef {{ name: string, branch: string, enabled: boolean, clean: boolean }} Repo
 */

/** @var {state} state */
const state = {
  /** @type {[Repo]} */
  repos: [],
  /** @type {string} date as string */
  repoFileLastUpdate: null,
  /** @type {number} number of started actions, 0 will mean no actions are running */
  performingActionCounter: 0,
};

const mutations = {
  UPDATE_REPOS(state, payload) {
    const currentRepos = state.repos;
    payload.forEach(element => {
      const index = currentRepos.map(repo => repo.name).indexOf(element.name);
      if (index > -1) {
        currentRepos[index] = Object.assign(currentRepos[index], element);
      } else {
        currentRepos.push(element);
      }
    });

    state.repos = Array.from(currentRepos);
  },
  INC_PERFORM_ACTION(state) {
    state.performingActionCounter += 1;
  },
  DEC_PERFORM_ACTION(state) {
    state.performingActionCounter -= 1;
  },
  SET_REPOFILE_LAST_UPDATE(state, payload) {
    state.repoFileLastUpdate = payload;
  },
};

const getters = {
  /**
   * @returns {[Repo]} the list of repos
   */
  repos: state => state.repos,
  /**
   * @returns {[Repo]} A list of enabled and not clean repos
   */
  dirtyAndEnabledRepos: state => state.repos.filter(repo => repo.enabled && !repo.clean),
  /**
   * @returns {string} the date of the last time repo file was checked
   */
  repoFileLastUpdate: state => state.repoFileLastUpdate,
  /**
   * @returns {boolean} wether a process interacting with the repos is active
   */
  performingAction: state => state.performingActionCounter > 0,
  /**
   * @param {state} state
   * @returns the repos grouped by level/branch
   */
  reposGroupedByBranch: state => {
    if (state.repos.length === 0) {
      return [];
    }
    const uniqueBranches = [...new Set(state.repos.map(repo => repo.branch))].filter(item => !!item);

    return uniqueBranches.map(branch => ({
      branch,
      repos: state.repos.filter(repo => repo.branch && repo.branch === branch),
    }));
  },
};

const actions = {
  /**
   * Gets the repos from the repo file, if they exist, it updates their enabled property
   * @param {*} store
   */
  reloadRepos({ commit }) {
    commit('INC_PERFORM_ACTION');
    const data = readFileSync(`${constants.ANYWHERE_META}repos.txt`, 'utf8');
    const metadata = statSync(`${constants.ANYWHERE_META}repos.txt`);
    commit('SET_REPOFILE_LAST_UPDATE', metadata.mtime);
    commit('UPDATE_REPOS', parseRepoFile(data));
    commit('DEC_PERFORM_ACTION');
  },
  /**
   * Perform a status command and updates repos
   * @param {*} store
   */
  reposStatus({ commit }) {
    commit('INC_PERFORM_ACTION');
    return new Promise((resolve, reject) => {
      execAsync(`cd ${constants.ANYWHERE_META} && ${constants.METASCRIPTS}/status.sh`).then(({ stderr, stdout }) => {
        if (stderr) {
          commit('DEC_PERFORM_ACTION');
          reject(stderr);
        } else {
          commit('UPDATE_REPOS', parseRepos(stdout));
          commit('DEC_PERFORM_ACTION');
          resolve();
        }
      });
    });
  },
  /**
   * Saves to disk the changes on repos to the repo file
   * @param {*} store
   * @param {{repo:string, enabled:boolean}} repo
   */
  saveRepoFile({ dispatch }, repo) {
    const findRepoOnFile = (repo, lines) =>
      lines.findIndex(line => {
        const name = line.split(':')[0].replace('#', '');
        return repo.repo === name;
      });

    const metadata = statSync(`${constants.ANYWHERE_META}repos.txt`);
    const data = readFileSync(`${constants.ANYWHERE_META}repos.txt`, 'utf8');
    const lines = data.split('\n');
    const index = findRepoOnFile(repo, lines);
    if (new Date(metadata.mtime) > new Date(this.getters.repoFileLastUpdate)) {
      const fileRepoEnabled = lines[index][0] !== '#';
      if (repo.enabled !== fileRepoEnabled) {
        console.error('Error, file modified and sync lost');
        return;
      }
    }

    if (repo.enabled) {
      lines[index] = `#${lines[index]}`;
    } else {
      lines[index] = lines[index].substr(1, lines[index].length);
    }

    writeFileSync(`${constants.ANYWHERE_META}repos.txt`, lines.join('\n'), { encoding: 'utf8' });
    dispatch('reloadRepos');
  },
  /**
   * Takes a list of custom repo objects and checks out to all of them
   * @param {*} store
   * @param {[{name: string, branch: string}]} repos
   */
  gitCheckout({ commit, dispatch }, repos) {
    commit('INC_PERFORM_ACTION');
    setTimeout(() => {
      const promises = [];
      repos.forEach(repo => {
        promises.push(
          new Promise(async resolve => {
            const existBranch = await dispatch('existBranchLocally', {
              repo: repo.name,
              branch: repo.branch,
            });
            let command;
            if (existBranch) {
              command =
                `cd ${constants.ANYWHERE_META} && ` +
                `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${repo.branch}`;
            } else {
              command =
                `cd ${constants.ANYWHERE_META} && ` +
                `${constants.METASCRIPTS}/repo.sh ${repo.name} fetch && ` +
                `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${repo.branch}`;
            }
            const child = exec(command);
            child.on('close', code => {
              console.log(`Finished repo.sh for branch ${repo.name} with code ${code}`);
              resolve();
              // TODO if close code error, reject
            });
          }),
        );
      });
      Promise.all(promises).then(() => {
        commit('DEC_PERFORM_ACTION');
        dispatch('reposStatus');
      });
    }, 1000);
  },
  /**
   * Checks out the repos supplied to the branch specified by the parameter
   * @param {*} store
   * @param {{ repos: {enabled:boolean, branch:string, name:string}, branch:string}} payload
   */
  gitAllCheckout({ commit, dispatch }, { repos, branch }) {
    commit('INC_PERFORM_ACTION');
    setTimeout(() => {
      const promises = [];
      repos.forEach(repo => {
        if (repo.enabled && repo.branch !== branch) {
          promises.push(
            new Promise(async resolve => {
              const existBranch = await dispatch('existBranchLocally', { repo: repo.name, branch });
              let command;
              if (existBranch) {
                command =
                  `cd ${constants.ANYWHERE_META} && ` +
                  `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${branch}`;
              } else {
                command =
                  `cd ${constants.ANYWHERE_META} && ` +
                  `${constants.METASCRIPTS}/repo.sh ${repo.name} fetch && ` +
                  `${constants.METASCRIPTS}/repo.sh ${repo.name} checkout ${branch}`;
              }
              const child = exec(command);
              child.stderr.on('data', data => {
                if (data.indexOf('POST-CHECKOUT') > -1) {
                  console.log(`Detected branch switch for repo ${repo.name}`);
                  repo.branch = branch;
                  commit('UPDATE_REPOS', [repo]);
                }
              });
              child.on('close', code => {
                console.log(`Finished repo.sh for branch ${repo.name} with code ${code}`);
                resolve();
                // TODO if close code error, reject
              });
            }),
          );
        }
      });
      Promise.all(promises).then(() => {
        commit('DEC_PERFORM_ACTION');
        dispatch('reposStatus');
      });
    }, 1000);
  },
  /**
   * Performs a fetch && merge command
   * @param {*} store
   */
  fetchAndMerge({ commit }, repos) {
    commit('INC_PERFORM_ACTION');
    if (repos) {
      const promises = [];
      repos.forEach(repo => {
        promises.push(
          new Promise(resolve => {
            execAsync(
              `cd ${constants.ANYWHERE_META} && ${constants.METASCRIPTS}/fetch.sh && ${
                constants.METASCRIPTS
              }/gitall.sh merge`,
            ).finally(() => {
              console.log('fetch && merge finished for repo', repo);
              resolve();
            });
          }),
        );
      });
      return Promise.all(promises).then(() => {
        commit('DEC_PERFORM_ACTION');
      });
    }

    return new Promise(resolve => {
      execAsync(
        `cd ${constants.ANYWHERE_META} && ${constants.METASCRIPTS}/fetch.sh && ${
          constants.METASCRIPTS
        }/gitall.sh merge`,
      )
        .then(() => {
          console.log('fetch && merge finished');
          resolve();
        })
        .finally(() => commit('DEC_PERFORM_ACTION'));
    });
  },
  /**
   * Checks if a branch exists locally without doing any request, just local git
   * @param {*} store
   * @param {{repo: string, branch: string}}} name of the repo and name of the branch
   */
  existBranchLocally({ commit }, { repo, branch }) {
    commit('INC_PERFORM_ACTION');
    return new Promise(resolve => {
      const child = exec(
        `cd ${constants.ANYWHERE_META} && ${constants.METASCRIPTS}/repo.sh ${repo} rev-parse --verify ${branch}`,
      );
      child.on('close', code => {
        commit('DEC_PERFORM_ACTION');
        resolve(code === 0);
      });
    });
  },
};

/** repos store */
export default {
  state,
  mutations,
  actions,
  getters,
};
