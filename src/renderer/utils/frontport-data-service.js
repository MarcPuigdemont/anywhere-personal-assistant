const data = {
  steps: [],
};

const DataService = {
  data,
  steps: () => data.steps,
  clearSteps: () => {
    data.steps = [];
  },
  addStep: (type, extra) => {
    data.steps.push({ name: `step-${data.steps.length}`, type, data: extra });
  },
  loadSteps(steps) {
    data.steps = steps;
  },
};

export default DataService;
