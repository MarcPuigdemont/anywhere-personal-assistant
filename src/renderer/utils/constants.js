import { readFileSync } from 'fs';

const constants = {
  METASCRIPTS: '',
  ANYWHERE_META: '',
  ANYWHERE_VERSIONS: '',
  ANYWHERE_FEATURE_BRANCHES: '',
  BB_USER: '',
  BB_PASSWORD: '',
  JIRA_USER: '',
  JIRA_PASSWORD: '',
  BRANCH_USER: '',
  JIRA_ACCOUNT_ID: '',
  USER_DATA_PATH: '',
  CONSTANTS_FILE_PATH: '',
};

constants.configure = (homePath, appName) => {
  const userDataPath = `${homePath}/Library/Application Support/${appName}`;
  const filePath = `${userDataPath}/settings.js`;

  let result = {};
  try {
    result = JSON.parse(readFileSync(filePath));
  } catch (err) {
    console.info('No settings file found');
    return;
  }

  constants.METASCRIPTS = result.metascriptsPath;
  constants.ANYWHERE_META = result.anywherePath;
  constants.ANYWHERE_VERSIONS = result.anywhereVersions;
  constants.ANYWHERE_FEATURE_BRANCHES = result.featureBranches;

  constants.BB_USER = result.bbUser;
  constants.BB_PASSWORD = result.bbPassword;
  constants.JIRA_USER = result.jiraUser;
  constants.JIRA_PASSWORD = result.jiraPassword;
  constants.BRANCH_USER = result.branchUser;
  constants.JIRA_ACCOUNT_ID = result.jiraAccountId;

  constants.USER_DATA_PATH = userDataPath;
  constants.CONSTANTS_FILE_PATH = filePath;
};

const versionComparator = (a, b) => {
  const versionA = constants.ANYWHERE_VERSIONS.findIndex(
    version => version.name === a || (a === 'develop' && version.isDevelop),
  );
  const versionB = constants.ANYWHERE_VERSIONS.findIndex(
    version => version.name === b || (b === 'develop' && version.isDevelop),
  );

  return versionA - versionB;
};

const branchesComparator = (a, b) => {
  const aIsTicketBranch = !a.startsWith('release/') && a !== 'develop';
  const bIsTicketBranch = !b.startsWith('release/') && b !== 'develop';

  if (aIsTicketBranch && bIsTicketBranch) {
    return a.localeCompare(b);
  } else if (aIsTicketBranch) {
    return -1;
  } else if (bIsTicketBranch) {
    return 1;
  }

  const startA = a.indexOf('/');
  const endA = a.indexOf('.', startA);
  const versionA = a === 'develop' ? a : a.substring(startA + 1, endA);

  const startB = b.indexOf('/');
  const endB = b.indexOf('.', startB);
  const versionB = b === 'develop' ? b : b.substring(startB + 1, endB);

  return versionComparator(versionA, versionB);
};

export { constants, versionComparator, branchesComparator };
