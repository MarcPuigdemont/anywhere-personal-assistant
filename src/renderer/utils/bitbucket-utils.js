function getPullRequests(prs, user) {
  const userPRs = prs.values.filter(pr => pr.author.username === user);
  return userPRs;
}

export default getPullRequests;
