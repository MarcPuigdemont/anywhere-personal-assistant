import DialogService from './dialog-service';

let store;
const DirtyDialogService = {
  setStore: s => {
    store = s;
  },

  openDialog: repos =>
    store.dispatch('reposStatus').then(() => {
      let reposToCheck = store.getters.repos;
      if (repos && repos.length > 0) {
        const suppliedReposNames = repos.map(repo => repo.name);
        reposToCheck = reposToCheck.filter(repo => suppliedReposNames.includes(repo.name));
      }
      const dirtyRepos = reposToCheck.filter(repo => repo.enabled && !repo.clean);

      if (dirtyRepos.length === 0) {
        return Promise.resolve();
      }

      const dirtyReposNames = dirtyRepos.map(repo => repo.name).join(', ');

      return DialogService.openDialog({
        title: 'The are some repos with changes',
        text: `This means you may probably run into issues on ${dirtyReposNames}`,
        ok: "Continue, I don't care",
        cancel: 'Cancel Action',
      });
    }),
};

export default DirtyDialogService;
