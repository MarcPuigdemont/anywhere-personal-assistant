import { exec } from 'child_process';

const execAsync = async command =>
  new Promise((resolve /* , reject */) => {
    exec(command, { maxBuffer: 1024 * 1500 }, (stderr, stdout) => {
      resolve({ stdout, stderr });
    });
  });

export default execAsync;
