let dialogComponent;
let resolve;
let reject;

let dialogTitle = 'Dialog title';
let dialogText = 'Dialog Text';
let dialogOk = 'Ok';
let dialogCancel = 'Cancel';

const DialogService = {
  setDialogComponent: dialog => {
    dialogComponent = dialog;
  },

  setText({ title = undefined, text = undefined, ok = undefined, cancel = undefined }) {
    if (title) dialogTitle = title;
    if (text) dialogText = text;
    if (ok) dialogOk = ok;
    if (cancel) dialogCancel = cancel;
  },

  openDialog: ({ title = undefined, text = undefined, ok = undefined, cancel = undefined } = {}) => {
    const promise = new Promise((res, rej) => {
      resolve = res;
      reject = rej;
    });
    dialogComponent.$set(dialogComponent, 'resolve', resolve);
    dialogComponent.$set(dialogComponent, 'reject', reject);
    dialogComponent.$set(dialogComponent, 'title', title || dialogTitle);
    dialogComponent.$set(dialogComponent, 'text', text || dialogText);
    dialogComponent.$set(dialogComponent, 'okText', ok || dialogOk);
    dialogComponent.$set(dialogComponent, 'cancelText', cancel || dialogCancel);
    dialogComponent.$set(dialogComponent, 'open', true);

    return promise;
  },
};

export default DialogService;
