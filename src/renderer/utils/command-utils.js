function getRepoLevel(branch) {
  const b = branch.toLowerCase();
  if (b.indexOf('edinburgh') > -1) {
    return 'Edinburgh';
  } else if (b.indexOf('frankfurt') > -1) {
    return 'Frankfurt';
  } else if (b.indexOf('granada') > -1) {
    return 'Granada';
  } else if (b.indexOf('honolulu') > -1) {
    return 'Honolulu';
  } else if (b.indexOf('develop') > -1) {
    return 'Develop';
  }

  return 'Unknown';
}

function parseRepos(repos) {
  const lines = repos.split('\n');
  lines.splice(0, 3);

  // lines = lines.filter(line => line !== '' && line[0] !== ' ');
  const result = [];

  lines.forEach(line => {
    if (line[0] === ' ') {
      if (result.length > 0) {
        // Previous repo has been modified
        result[result.length - 1].clean = false;
      }
    } else {
      let fields = line.split(' ');
      fields = fields.filter(field => field !== '');
      if (fields.length > 1) {
        result.push({
          name: fields[0].replace('anywhere-', '').replace('mobile-', ''),
          branch: fields[1],
          level: getRepoLevel(fields[1]),
          clean: true,
        });
      }
    }
  });

  console.log(result);

  return result;
}

function getFileModifiedDate(metadata) {
  return new Date(metadata.mtime);
}

function parseRepoFile(data) {
  let lines = data.split('\n');
  lines = lines.filter(line => line !== '' && !(line[0] === '#' && line[1] === ' '));

  return lines.map(line => {
    const fields = line.split(':');
    const enabled = fields[0][0] !== '#';
    return {
      enabled,
      repo: fields[0].replace('#', ''),
      name: fields[0]
        .replace('anywhere-', '')
        .replace('mobile-', '')
        .replace('#', ''),
      intendedLevel: fields[1],
    };
  });
}

function parseJiraResponse(response) {
  const index = response.indexOf('{');
  return JSON.parse(response.substr(index, response.length));
}

export { parseRepos, parseRepoFile, getFileModifiedDate, parseJiraResponse };
