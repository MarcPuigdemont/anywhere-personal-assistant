import Vue from 'vue';
import axios from 'axios';
import Vuetify from 'vuetify';

import 'vuetify/dist/vuetify.css';

import App from './App';
import router from './router';
import store from './store';

import DirtyDialogService from './utils/dirty-dialog-service';

import PullRequests from './components/PullRequests.vue';
import Tickets from './components/Tickets.vue';
import Frontport from './components/Frontport.vue';
import Settings from './components/Settings.vue';

Vue.use(Vuetify);
if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  components: {
    App,
    PullRequests,
    Tickets,
    Frontport,
    Settings,
  },
  router,
  store,
  template: '<App/>',
}).$mount('#app');

DirtyDialogService.setStore(store);

store.dispatch('reloadRepos');
